package ijn;


import ijn.model.FeatureVector;
import org.junit.Assert;
import org.junit.Test;


import java.util.List;

import static ijn.model.FeatureVector.*;
import static java.util.Arrays.*;
import static java.util.stream.Collectors.*;

public class Tests {

    public static final double EPS = 0.001;

    @Test
    public void cosineSimilarity_scaled() {

        List<Double> values = asList(1.0, 22.0, -25.5, 0.0, 12.0);
        List<Double> doubled = values.stream().map(x -> 2 * x).collect(toList());
        FeatureVector v1 = new FeatureVector(values);
        FeatureVector v2 = new FeatureVector(doubled);

        double actual = cosineSimilarity(v1, v2);

        Assert.assertEquals(1.0, actual, EPS);
    }

    @Test
    public void cosineSimilarity_orthogonal() {
        FeatureVector v1 = new FeatureVector(asList(1.0, 0.0));
        FeatureVector v2 = new FeatureVector(asList(0.0, 17.0));

        double actual = cosineSimilarity(v1, v2);

        Assert.assertEquals(0.0, actual, EPS);
    }
}
