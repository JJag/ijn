package ijn.services;

import ijn.model.Doc;
import ijn.model.TermPair;
import ijn.util.FileUtils;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

public class TermExtractor {
    public static List<TermPair> extractTerms(Document ccl) throws IOException {
        List<String> punctuations = asList(".", ",", " ", ":", "?", "!", ";", "(", ")", "[", "]", "-");
        String stopwordsString = FileUtils.readFile("src/main/resources/stopwords.txt");
        List<String> stopwords = asList(stopwordsString.split(", "));

        List<TermPair> words = ccl.getElementsByTag("tok")
                .stream()
                .map(tok -> {
                    String word = tok.getElementsByTag("base").text();
                    Long synsetId = null;
                    Elements prop = tok.getElementsByAttributeValue("key", "sense:ukb:syns_id");
                    if (!prop.isEmpty())
                        synsetId = Long.valueOf(prop.text());
                    return new TermPair(word, synsetId);
                })
                .filter(pair -> !stopwords.contains(pair.term))
                .filter(pair -> !punctuations.contains(pair.term))
                .collect(toList());
        return words;
    }
}
