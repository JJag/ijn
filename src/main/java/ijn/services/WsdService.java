package ijn.services;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

/**
 * Interfejs do komunikacji z WSD
 */
public class WsdService {

    private static final String nlprestURL = "http://ws.clarin-pl.eu/nlprest/base/";

    public static Document process(String d) {
        try {
            String id = post(d);
//            id = nlpProcess("wcrft2", id);
            id = nlpProcess("wsd2", id);
            String s = get(id);
            return Jsoup.parse(s, "", Parser.xmlParser());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static String post(String d) throws IOException {
        return ClientBuilder
                .newClient()
                .target(nlprestURL + "upload")
                .request()
                .post(Entity.entity(ijn.util.FileUtils.createTempFile(d), "binary/octet-stream"))
                .readEntity(String.class);
    }

    private static String get(String id) throws IOException {
        URL url = new URL(nlprestURL + "downloadbin/" + id);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("GET");
        if (conn.getResponseCode() == 200) {
            InputStream is = conn.getInputStream();
            return IOUtils.toString(is, "UTF-8");
        } else {
            throw new IOException("Error downloading file");
        }
    }

    private static String getResponse(Response res) throws IOException {
        if (res.getStatus() != 200) {
            throw new IOException("Error in nlprest processing");
        }
        return res.readEntity(String.class);
    }

    private static String nlpProcess(String toolname, String id) throws IOException, InterruptedException {
        Client client = ClientBuilder.newClient();
        String taskId = getResponse(client.target(nlprestURL + "startTask/" + toolname + "/" + id).request().get());
        String status = "";
        JSONObject jsonResponse = new JSONObject();
        while (!status.equals("DONE")) {
            String res = getResponse(client.target(nlprestURL + "getStatus/" + taskId).request().get());
            jsonResponse = new JSONObject(res);
            status = jsonResponse.getString("status");
            if (status.equals("ERROR")) {
                throw new IOException("Error in processing");
            }
            Thread.sleep(500);
        }
        return jsonResponse.getString("value");
    }

}
