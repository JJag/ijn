package ijn.services;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class WordNetService {

    private static String dbPath = "plwordnet2.sqlite";

    private static Connection c = null;

    public static void init() {
        try {

            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:/" + dbPath);
            c.setAutoCommit(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Set<Long> getHyponyms(Long synsetId) {
        HashSet<Long> results = new HashSet<>();
        try {
            Statement stmt = c.createStatement();
            String query = "SELECT PARENT_ID FROM synsetrelation WHERE REL_ID = 10 AND CHILD_ID = ";
            query += synsetId;
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Long parentId = rs.getLong("PARENT_ID");
                results.add(parentId);
            }
            rs.close();
            stmt.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return results;
    }

    public static void close() {
        try {
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
