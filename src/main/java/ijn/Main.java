package ijn;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import ijn.services.WsdService;
import ijn.util.FileUtils;
import java.io.File;
import java.io.FileOutputStream;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class Main {

    public static void main(String[] args) throws IOException {

//        List<String> punctuations = asList(".", ",", " ", ":", "?", "!", ";");
//        String example = FileUtils.readFile("src/main/resources/test.xml");
//        Document ccl = WsdService.process(example);
//
//        String stopwordsString = FileUtils.readFile("src/main/resources/stopwords.txt");
//        List<String> stopwords = asList(stopwordsString.split(", "));
//
//        List<String> words = ccl.getElementsByTag("base")
//                .stream()
//                .map(Element::text)
//                .filter(word -> !stopwords.contains(word))
//                .filter(word -> !punctuations.contains(word))
//                .collect(Collectors.toList());
//
//        System.out.println(ccl.outerHtml());
//
//        words.forEach(System.out::println);
        
        preprocessAllData();
    }

    public static void saveResultAsFile() {
        try {
            String resultFolderPath = "src/main/resources/results/";
            String dataFolderPath = "src/main/resources/data/";

            String example = FileUtils.readFile(dataFolderPath + "1" + ".txt");
            Document ccl = WsdService.process(example);
            FileUtils.saveFile(resultFolderPath + "1" + ".xml", ccl.outerHtml());
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void preprocessAllData() {
        String[] foldersToProcess = {"nauka i popularno_naukowe_i_podręczniki", "religijne", "techniczne", "urzędowe i ustawy"};
        String pathToData = "src/main/resources/data sources/";
        String pathToResult = "src/main/resources/data preprocessed/";
        for (String folderName : foldersToProcess) {
            File dir = new File(pathToData + folderName);
            File[] files = dir.listFiles((d, name) -> name.endsWith(".xml"));
            for (File file : files) {
                List<String> punctuations = asList(".", ",", " ", ":", "?", "!", ";");
                String doc = null;
                try {
                    doc = FileUtils.readFile(file.getPath());
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
                Document ccl = WsdService.process(doc);

                String stopwordsString = null;
                try {
                    stopwordsString = FileUtils.readFile("src/main/resources/stopwords.txt");
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }

                try {
                    FileUtils.saveFile(pathToResult + folderName + "/" +file.getName(), ccl.outerHtml());
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }

                List<String> stopwords = asList(stopwordsString.split(", "));

                List<String> words = ccl.getElementsByTag("base")
                        .stream()
                        .map(Element::text)
                        .filter(word -> !stopwords.contains(word))
                        .filter(word -> !punctuations.contains(word))
                        .collect(Collectors.toList());

                System.out.println(ccl.outerHtml());

                words.forEach(System.out::println);
            }
        }
    }
}
