package ijn;

import ijn.model.Doc;
import ijn.model.Model;
import ijn.model.TermPair;
import ijn.services.WordNetService;
import ijn.services.WsdService;
import ijn.util.FileUtils;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

public class ClassifyTest {


    public static void main(String[] args) throws IOException {


        Doc c1 = extractTerms(FileUtils.readFile("src/main/resources/sport.txt"));
        Doc c2 = extractTerms(FileUtils.readFile("src/main/resources/moto.txt"));
        Doc c3 = extractTerms(FileUtils.readFile("src/main/resources/programowanie.txt"));
        Doc unknown = extractTerms(FileUtils.readFile("src/main/resources/example.txt"));

        System.out.println("wczytano");
        Map<String, Doc> categories = new HashMap<>();
        categories.put("sport", c1);
        categories.put("moto", c2);
        categories.put("programowanie", c3);

        WordNetService.init();

        Model model = new Model(categories, 1000);
        String label = model.classify(unknown);

        System.out.println(label);

        WordNetService.close();
    }

    public static Doc extractTerms(String s) throws IOException {

        Document ccl = WsdService.process(s);
        List<String> punctuations = asList(".", ",", " ", ":", "?", "!", ";", "(", ")", "[", "]", "-");
        String stopwordsString = FileUtils.readFile("src/main/resources/stopwords.txt");
        List<String> stopwords = asList(stopwordsString.split(", "));


        List<TermPair> words = ccl.getElementsByTag("tok")
                .stream()
                .map(tok -> {
                    String word = tok.getElementsByTag("base").text();
                    Long synsetId = null;
                    Elements prop = tok.getElementsByAttributeValue("key", "sense:ukb:syns_id");
                    if (!prop.isEmpty())
                        synsetId = Long.valueOf(prop.text());
//                    System.out.println("(" + word + ", " + synsetId + ")");
                    return new TermPair(word, synsetId);
                })
                .filter(pair -> !stopwords.contains(pair.term))
                .filter(pair -> !punctuations.contains(pair.term))
                .collect(toList());
        return new Doc(words);
    }
}
