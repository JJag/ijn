package ijn.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static ijn.util.CollectionUtils.intersection;
import static java.lang.Math.sqrt;

public class FeatureVector {

    Map<Object, Double> features;

    public FeatureVector(Map<Object, Double> features) {
        this.features = features;
    }
    public FeatureVector(List<Double> vector) {
        features = new HashMap<>();
        for(int i = 0; i < vector.size(); i++) {
            features.put(i, vector.get(i));
        }
    }

    public double value(Object feature) {
        return features.getOrDefault(feature, 0.0);
    }
    public Set<Object> keys() {
        return features.keySet();
    }

    public double length() {
        double lengthSquared = features.values().stream()
                .mapToDouble(x -> x * x)
                .sum();
        return sqrt(lengthSquared);
    }

    public static double cosineSimilarity(FeatureVector v1, FeatureVector v2) {
        double numerator = intersection(v1.keys(), v2.keys())
                .stream()
                .mapToDouble(f -> v1.value(f) * v2.value(f))
                .sum();
        double denominator = v1.length() * v2.length();
        return numerator / denominator;
    }
}
