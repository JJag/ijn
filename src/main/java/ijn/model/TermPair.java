package ijn.model;

public class TermPair {

    public final String term;
    public final Long synsetId;

    public TermPair(String term, Long synsetId) {
        this.term = term;
        this.synsetId = synsetId;
    }
}
