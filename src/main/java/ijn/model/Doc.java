package ijn.model;

import ijn.services.WordNetService;

import java.util.*;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class Doc {

    private Map<String, Long> termFrequencies;
    private Map<Long, Long> conceptFrequencies;

    public Doc(Collection<TermPair> words) {
        termFrequencies = words.stream()
                .map(p -> p.term)
                .collect(groupingBy(identity(), counting()));
        conceptFrequencies = words.stream()
                .filter(w -> w.synsetId != null)
                .map(p -> p.synsetId)
                .collect(groupingBy(identity(), counting()));
    }

    public double tf(String term) {
        return termFrequencies.getOrDefault(term, 0L);
    }

    public double cf(long sysnetId) {
        return conceptFrequencies.getOrDefault(sysnetId, 0L);
    }

    public double tf(Object feature) {
        if (feature instanceof String)
            return tf((String)feature);
        else
            return cfWithHyponyms((Long) feature);
    }

    public double cfWithHyponyms(long sysnetId) {
        Set<Long> hyponyms = WordNetService.getHyponyms(sysnetId);
        double cfHyponyms = hyponyms.stream()
                .mapToDouble(h -> cf(h))
                .sum();
        return cf(sysnetId) + cfHyponyms;
    }

    public Set<Object> features() {
        Set<Object> features = new HashSet<>();
        features.addAll(termFrequencies.keySet());
        features.addAll(conceptFrequencies.keySet());
        return features;
    }

    public void pruneFeatures(List<Object> relevantFeatures) {
        termFrequencies.entrySet().removeIf(entry -> !relevantFeatures.contains(entry.getKey()));
        conceptFrequencies.entrySet().removeIf(entry -> !relevantFeatures.contains(entry.getKey()));
    }
}
