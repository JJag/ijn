package ijn.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ijn.util.MathUtils.sqaure;
import static java.lang.Math.log;
import static java.lang.Math.signum;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

public class Model {

    private final Map<String, Doc> categories;
    private Map<String, FeatureVector> categoryVectors;

    public Model(Map<String, Doc> categories, int featureVectorSize) {
        this.categories = categories;
        categoryVectors = new HashMap<>();

        List<Object> featureList = categories.values()
                .stream()
                .flatMap(d -> d.features().stream())
                .distinct()
                .collect(toList());
        List<String> categoryList = new ArrayList<>(categories.keySet());
        int n = featureList.size();
        int m = categoryList.size();
        double[][] freqMatrix = new double[n + 1][m + 1];
        for (int j = 0; j < n; j++) {
            for (int k = 0; k < m; k++) {
                Doc cat = categories.get(categoryList.get(k));
                double N_ij = cat.tf(featureList.get(j));
                freqMatrix[j][k] = N_ij;
                freqMatrix[n][k] += N_ij;
                freqMatrix[j][m] += N_ij;
            }
            freqMatrix[n][m] += freqMatrix[j][m];
        }
        double[][] C = new double[n][m];
        for (int j = 0; j < n; j++) {
            for (int k = 0; k < m; k++) {
                double N = freqMatrix[n][m];
                double f_jk = freqMatrix[j][k] / N;
                double f_j = freqMatrix[j][m] / N;
                double f_k = freqMatrix[n][k] / N;
                C[j][k] = N * (sqaure(f_jk - f_j * f_k) / (f_j * f_k)) * signum(f_jk - f_j * f_k);
            }
        }
        Map<String, List<Pair<Integer, Double>>> feats = new HashMap<>();
        for (int k = 0; k < m; k++) {
            final String category = categoryList.get(k);
            List<Pair<Integer, Double>> list = new ArrayList<>();
            feats.put(category, list);
            for (int j = 0; j < n; j++) {
                list.add(new Pair<>(j, C[j][k]));
            }
            list.sort((o1, o2) -> o2.snd.compareTo(o1.snd));
            list = list.subList(0, Math.min(list.size(), featureVectorSize));
            List<Object> relevantFeatures = list.stream()
                    .map(pair -> featureList.get(pair.fst))
                    .collect(toList());
            categories.get(categoryList.get(k)).pruneFeatures(relevantFeatures);

            Map<Object, Double> featureVector = relevantFeatures
                    .stream().collect(toMap(f -> f,
                            f -> tfidf(f, category)));
            categoryVectors.put(category, new FeatureVector(featureVector));

//            System.out.println(category.toUpperCase());
//            featureVector.forEach((key, v) -> System.out.println(key + " -> " + v));
//            System.out.println();
        }


    }

    private double termIdf(String term) {
        final int N = categories.size();
        double df = categories.values().stream()
                .filter(c -> c.tf(term) > 0)
                .count();
        if (df > 0)
            return log(N / df);
        else
            return 0;   // sztuczka, żeby nowe słowa z dokumentów do rozpoznania były ignorowane
    }

    private double conceptIdf(long synsetId) {
        final int N = categories.size();
        double df = categories.values().stream()
                .filter(c -> c.cf(synsetId) > 0)
                .count();
        if (df > 0)
            return log(N / df);
        else
            return 0;
    }

    public double idf(Object feature) {
        if (feature instanceof String)
            return termIdf((String) feature);
        else
            return conceptIdf((Long) feature);
    }

    private double termTfidf(String term, String category) {
        Doc c = categories.get(category);
        return c.tf(term) * termIdf(term);
    }

    public double conceptTfidf(long synsetId, String category) {
        Doc c = categories.get(category);
        return c.cfWithHyponyms(synsetId) * conceptIdf(synsetId);
    }

    public double tfidf(Object feature, String category) {
        if (feature instanceof String)
            return termTfidf((String) feature, category);
        else
            return conceptTfidf((Long) feature, category);
    }

    public String classify(Doc d) {
        String bestCategory = null;
        Double maxCos = Double.MIN_VALUE;

        Map<Object, Double> features = d.features()
                .stream().collect(toMap(f -> f,
                        f -> (d.tf(f) * idf(f))));

        FeatureVector docVector = new FeatureVector(features);

        for (String cat : categories.keySet()) {
            FeatureVector catVector = categoryVectors.get(cat);
            double cos = FeatureVector.cosineSimilarity(docVector, catVector);
            System.out.println("DIST TO " + cat + " = " + cos);
            if (cos > maxCos) {
                maxCos = cos;
                bestCategory = cat;
            }
        }
        return bestCategory;
    }
}
