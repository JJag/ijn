package ijn.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author JJag
 */
public class FileUtils {

    /**
     * Tworzy plik tymczasowy zawierający podany string
     *
     * @param content zawartość pliku
     */
    public static File createTempFile(String content) throws IOException {
        File temp = File.createTempFile("tempfile", ".tmp");
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(temp), "UTF-8"));
        bw.write(content);
        bw.close();
        return temp;
    }

    public static String readFile(String path)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, "UTF-8");
    }
    
    public static File saveFile(String path, String content) throws IOException {
        File file = new File(path);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
        bw.write(content);
        bw.close();
        return file;
    }
}
