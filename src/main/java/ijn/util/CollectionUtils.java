package ijn.util;

import java.util.HashSet;
import java.util.Set;

public class CollectionUtils {
    public static <T> Set<T> union(Set<T> setA, Set<T> setB) {
        Set<T> tmp = new HashSet<T>(setA);
        tmp.addAll(setB);
        return tmp;
    }

    public static <T> Set<T> intersection(Set<T> setA, Set<T> setB) {
        Set<T> tmp = new HashSet<T>(setA);
        tmp.retainAll(setB);
        return tmp;
    }
}
