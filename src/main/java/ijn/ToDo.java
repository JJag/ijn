package ijn;

import ijn.model.Doc;
import ijn.model.Model;
import ijn.model.TermPair;
import org.jsoup.nodes.Document;

import java.util.List;
import java.util.Map;

public class ToDo {
    public static void main(String[] args) {

        // na początku jakoś podzielić dane na treningowe i testowe


        Map<String, List<Document>> cclByCategory = null;    // CCL-ki pogrupowane po kategoriach
        Map<String, List<TermPair>> termsByCategory = null;  // dla każdego Documentu wyciągnięte listy termów TermExtractor.extractTerms()
        Map<String, Doc> categories = null;                  // Doci zrobione konstruktorem z List<TermPair>

        Map<String, List<Doc>> testDocs = null;             // dane testowe - tutaj Doc to jeden dokument, a nie kategoria!

        int maxFeatureVectorSize = 500;     // parametr do zbadania
        Model model = new Model(categories, maxFeatureVectorSize);

        // tutaj poklasyfikowac cały zbiór testowy i policzyć jakość klasyfikacji
        String predictedCategory = model.classify(testDocs.get("JAKAS KATEGORIA").get(42));    // tak sie klasyfikuje


        // CO PRZEBADAC
        // - różne maxFeatureVectorSize
        // - bez używania hiponimów - można np. przerobić WordNEtService, żeby zawsze zwracał pusty zbiór
        // - bez wordnetu - można spróbować wykomentować 52 linijke w klasie Doc, ale nie wiem czy to nie rozwali programu

    }
}
