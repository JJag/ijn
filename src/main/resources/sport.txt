Praktycznie we wszystkich przedmeczowych zapowiedziach, więcej szans na wygraną dawano gospodarzom. Nie mogło jednak być inaczej, skoro na boisko lidera tabeli przyjechała drużyna plasująca się w środkowej części stawki.

Inter faktycznie miał więcej z gry, jednak cóż z tego, jeśli nie miało to żadnego realnego przełożenia na ilości dogodnych okazji podbramkowych.
Ostatecznie, mimo roszad dokonywanych przez Roberto Manciniego, Interowi nie udało się przechylić szali zwycięstwa na swoją stronę.

Co więcej, kiedy wydawało się, że mecz zakończy się bezbramkowym remisem, Miranda stracił głowę we własnym polu karnym i sprokurował "jedenastkę" dla rywali. Pewnym egzekutorem karnego okazał się Domenico Berrardi i Sassuolo odniosło niespodziewane zwycięstwo na San Siro.